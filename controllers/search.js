const config = require('../config/config');
const recentController = require('../controllers/recent');
const Search = require('bing.search');
const util = require('util');

exports.getSearch = function(req, res) {
    var search = new Search(config.bingKey);

    if (req.query.offset == null) {
        search.images(req.params.searchTerm, {
            top: 10
        }, function(err, results) {
            if (err) throw err;
            var result = [];
            for (var i = 0; i < results.length; i++) {
                var obj = {};
                obj.thumbnail = results[i].thumbnail.url;
                obj.snippet = results[i].title;
                obj.url = results[i].sourceUrl;
                obj.context = results[i].url;

                result.push(obj);
            }

            recentController.addRecent(req.params.searchTerm);
            res.json(result);
        });
    }
    else {
        var toSkip = req.query.offset - 10;
        var byTen = toSkip * 10
        
        search.images(req.params.searchTerm, {
        top: 10,
        skip: req.query.offset * 10
    }, function(err, results) {
        if (err) throw err;
        var result = [];
        for (var i = 0; i < results.length; i++) {
            var obj = {};
            obj.thumbnail = results[i].thumbnail.url;
            obj.snippet = results[i].title;
            obj.url = results[i].sourceUrl;
            obj.context = results[i].url;

            result.push(obj);
        }

        recentController.addRecent(req.params.searchTerm);
        res.json(result);
    });
    }
};
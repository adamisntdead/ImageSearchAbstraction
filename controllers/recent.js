const Recent = require('../models/Recent.js');

exports.getRecent = function(req, res) {
    Recent
        .find({})
        .limit(10)
        .exec(function(err, posts) {
            if (err) {
                throw err;
            }
            var output = [];
            for (var i = 0; i < posts.length; i++) {
                var obj = {};
                obj.term = posts[i].term;
                
                output.push(obj);
            }
            res.json(output);
        });
};

exports.addRecent = function(term) {
    var recent = new Recent();
    recent.term = term;
    
    recent.save(function(err){
        if (err) throw err;
    });
};
// Require NPM Modules
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const config = require('./config/config.js');
const path = require('path');
const logger = require('morgan');
const compression = require('compression');

// Setup App
var app = express();
app.use(express.static(path.join(__dirname, 'public')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(compression());
require('./routes/routes.js')(app);


// Mongoose Connection
mongoose.connect(config.MongoHost);
mongoose.connection.on('error', function() {
  console.log('MongoDB Connection Error. Please make sure that MongoDB is running.');
  process.exit(1);
});

app.listen(config.port, function(){
   console.log('Listning on port: ' + config.port); 
});
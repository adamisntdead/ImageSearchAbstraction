// Require Modules
var mongoose = require('mongoose');

// Define our Agreements Schema
var RecentSchema = new mongoose.Schema({
    term: String
});

// Export the Schema
module.exports = mongoose.model('Recent', RecentSchema);
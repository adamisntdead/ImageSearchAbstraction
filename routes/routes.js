const recentController = require('../controllers/recent');
const searchController = require('../controllers/search');

module.exports = function(app) {
   app.get('/api/latest/', recentController.getRecent);
   app.get('/api/search/:searchTerm', searchController.getSearch);
};